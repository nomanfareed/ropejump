﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public Transform Target;
    public float DefaultFieldOfView;
    public float ExpandableFieldOfView;
    public float CameraApproachingSpeed;
    // Update is called once per frame
    void LateUpdate()
    {
        //float stepY = Mathf.LerpUnclamped(transform.position.y, Target.position.y / 2, Time.deltaTime * CameraApproachingSpeed);
        transform.position = new Vector3(transform.position.x, Target.position.y / 2, transform.position.z);
        float stepFOV = transform.position.y / 15;
        stepFOV = DefaultFieldOfView + (ExpandableFieldOfView * stepFOV);
        GetComponent<Camera>().fieldOfView = stepFOV;

        //GetComponent<Camera>().fieldOfView = Mathf.LerpUnclamped(GetComponent<Camera>().fieldOfView, stepFOV, Time.deltaTime * CameraApproachingSpeed);
    }
}
