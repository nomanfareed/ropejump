﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitInfo : MonoBehaviour
{
    public float Min;
    public float Max;
}
