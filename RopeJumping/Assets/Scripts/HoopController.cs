﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoopController : MonoBehaviour
{
    private void OnTriggerEnter(Collider collider)
    {
        GetComponent<Collider>().enabled = false;
        GetComponent<Animation>().Play();
    }
}
