﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InformationController : MonoBehaviour
{
    public Vector3 CameraTargetPos;
    public Vector3 CameraTargetRot;
}
