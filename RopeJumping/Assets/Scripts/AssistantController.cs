﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssistantController : MonoBehaviour
{
    public static AssistantController Instance;
    public GameObject Assistant1;
    Animator Animator1;
    public GameObject Assistant2;
    Animator Animator2;
    float TargetTime = 0.5f;
    public GameObject Rope;
    Coroutine lookThread;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        StartCoroutine("LateStart");
    }
    IEnumerator LateStart()
    {
        yield return new WaitForEndOfFrame();

        Animator1 = Assistant1.GetComponent<Animator>();
        Animator1.speed = 0;
        Animator2 = Assistant2.GetComponent<Animator>();
        Animator2.speed = 0;

        Animator1.Play("Pull", 0, TargetTime);
        Animator2.Play("Pull", 0, TargetTime);
        
        yield return new WaitForEndOfFrame();

        Rope.SetActive(true);
    }

    private void Update()
    {
        if (InputController.Instance.IsUserUsed || !Rope.activeSelf)
        {
            return;
        }
            TargetTime = Mathf.Lerp(TargetTime, 0.6f, Time.deltaTime * 6);
            Animator1.Play("Pull", 0, TargetTime);
            Animator2.Play("Pull", 0, TargetTime);
        //Assistant1TimeIndex = Mathf.Lerp(Assistant1TimeIndex, 0.0f, Time.deltaTime * 6);
        //Assistant1TimeIndex = Mathf.Clamp01(Assistant1TimeIndex);
        //Animator1.Play("Pull", 0, Assistant1TimeIndex);

        //Assistant2TimeIndex = Mathf.Lerp(Assistant2TimeIndex, 0.0f, Time.deltaTime * 6);
        //Assistant2TimeIndex = Mathf.Clamp01(Assistant2TimeIndex);
        //Animator2.Play("Pull", 0, Assistant2TimeIndex);
    }
    public void RecieveInputs(float factor)
    {
        TargetTime = Mathf.Clamp(factor,0,0.5f);
        Animator1.Play("Pull", 0, TargetTime);
        Animator2.Play("Pull", 0, TargetTime);
        //Animator1.Play("Pull", 0, Mathf.Clamp01(factor));
        //Animator2.Play("Pull", 0, Mathf.Clamp01(factor));

        //Assistant1TimeIndex += factor;
        //Assistant1TimeIndex = Mathf.Clamp01(Assistant1TimeIndex);
        //Animator1.Play("Pull",0, Assistant1TimeIndex);

        //Assistant2TimeIndex += factor;
        //Assistant2TimeIndex = Mathf.Clamp01(Assistant2TimeIndex);
        //Animator2.Play("Pull", 0, Assistant2TimeIndex);
    }
    //IEnumerator LookTowards(float time)
    //{
    //    Animator1.speed = 1;
    //    Animator1.speed = Animator1.speed / (time * 2);
    //    Animator1.Play("look", 0, 0);
    //    yield return new WaitForSeconds(time * 2);
        

    //}
}
