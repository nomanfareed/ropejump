﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform[] Regions;
    public Transform[] RegionCameraReference;
    public Transform Default;
    Transform TargetPos;
    Transform TargetRot;
    int speed = 2;

    private void Start()
    {
        TargetPos = Default;
        TargetRot = Default;
    }
    private void Update()
    {
        Vector3 currentPos = transform.localPosition;
        transform.localPosition = Vector3.Lerp(currentPos, TargetPos.localPosition, Time.deltaTime * speed);
        Vector3 currentRot = transform.localEulerAngles;
        Vector3 rotation = TargetRot.localEulerAngles;
        NormalizeEulerAngles(ref currentRot, ref rotation);
        transform.localEulerAngles = Vector3.Lerp(currentRot, rotation, Time.deltaTime * speed);
    }

    public void ZoomOut(float t, int regionIndex)
    {
        speed = 2;
        if (regionIndex < 0)
        {
            TargetPos = RegionCameraReference[Regions.Length - 1];
            TargetRot = RegionCameraReference[Regions.Length - 1];
        }
        else
        {
            TargetPos = RegionCameraReference[regionIndex];
            TargetRot = RegionCameraReference[regionIndex];
        }
    }
    public void ZoomIn()
    {
        speed = 1;
        TargetPos = Default;
        TargetRot = Default;
    }
    void NormalizeEulerAngles(ref Vector3 ReferenceAngle,ref Vector3 TargetAngle)
    {
        if (Mathf.Abs(ReferenceAngle.x - TargetAngle.x) > 180)
        {
            if(TargetAngle.x < ReferenceAngle.x)
            {
                TargetAngle = new Vector3(TargetAngle.x + 360,0,0);
            }
            else
            {
                ReferenceAngle = new Vector3(ReferenceAngle.x + 360,0,0);
            }
        }
    }

}
