﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public static InputController Instance;
    [HideInInspector]
    public bool IsUserUsed = false;
    Vector3 lastPos;
    Vector3 CurrentPos;
    float ReferenceStep;
    public bool Strechable = true, Moveable = false;
    
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        ReferenceStep = Screen.height / 5;
    }

    // Update is called once per frame
    void Update()
    {
        if(!(Input.mousePosition.x >= 0 && Input.mousePosition.x <= Screen.width &&
           Input.mousePosition.y >= 0 && Input.mousePosition.y <= Screen.height))
        {
            return;
        }
        if (Strechable)
        {
            if (Input.GetMouseButtonDown(0))
            {
                IsUserUsed = true;
                RopePullerController.Instance.DragRope();
                CurrentPos = lastPos = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                CurrentPos = Input.mousePosition;
                float TravelledDistance = CurrentPos.y - lastPos.y;
                float Result = TravelledDistance / ReferenceStep;
                RopePullerController.Instance.StretchRope(Result);
                lastPos = Input.mousePosition;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                RopePullerController.Instance.LeaveRope();
                IsUserUsed = false;
                lastPos = Input.mousePosition;
            }
        }
        else if (Moveable)
        {
            if (Input.GetMouseButtonDown(0))
            {
                CurrentPos = lastPos = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                CurrentPos = Input.mousePosition;
                float TravelledDistance = CurrentPos.x - lastPos.x;
                float Result = TravelledDistance / ReferenceStep;
                PlayerController.Instance.OnHorizontalMovement(Result);
                //RopePullerController.Instance.StretchRope(Result);
                lastPos = Input.mousePosition;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                lastPos = Input.mousePosition;
            }
        }
    }
    
}
