﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;
    Rigidbody Body;
    Animator Model;

    void Start()
    {
        Instance = this;
        Body = GetComponent<Rigidbody>();
        Model = transform.GetChild(0).GetComponent<Animator>();
    }
    
    public void OnCollisionDetach(bool Animateable)
    {
        CancelInvoke();
        float MaximumHeightGain = GetDistance(Body.velocity.y,Physics.gravity.y) + Body.transform.position.y;
        //int RegionIndex = GetRegionIndex(MaximumHeightGain);
        //if (RegionIndex < 0)
        //{
        //    float S = Camera.main.GetComponent<CameraController>().Regions[Camera.main.GetComponent<CameraController>().Regions.Length - 1].position.y - Body.transform.position.y;
        //    Physics.gravity = new Vector3(0, GetRequiredGravity(Body.velocity.y, S), 0);
        //}
        
        float time = GetTimeUp(Body.velocity.y, Physics.gravity.y);
        if (Animateable) { OnCharacterAnimation(time); }
        Invoke("NormalizeGravity", time);
        //Camera.main.GetComponent<CameraController>().ZoomOut(time,RegionIndex);
    }

    public void OnHorizontalMovement(float factor)
    {
        LimitInfo Limits = GetComponent<LimitInfo>();
        transform.position = new Vector3(Mathf.Clamp(transform.position.x + factor,Limits.Min,Limits.Max), transform.position.y, transform.position.z);
    }

    float GetDistance(float Vi,float g)
    {
        return -Mathf.Pow(Vi, 2) / (2 * g);
    }
    float GetTimeUp(float Vi, float g)
    {
        return -Vi / g;
    }
    float GetTimeDown(float S, float g)
    {
        return Mathf.Sqrt((2*S)/Mathf.Abs(g));
    }
    float GetRequiredGravity(float Vi,float S)
    {
        return -Mathf.Pow(Vi, 2) / (2 * S);
    }
    int GetRegionIndex(float MaxHeightGain)
    {
        Transform[] regions = Camera.main.GetComponent<CameraController>().Regions;
        for(int i = 0; i < regions.Length; i++)
        {
            if(regions[i].position.y > MaxHeightGain)
            {
                return i;
            }
        }
        return -1;
    }
    void NormalizeGravity()
    {
        Physics.gravity = new Vector3(0, -9.81f, 0);
        float t = GetTimeDown(Body.transform.position.y,Physics.gravity.y);
        InputController.Instance.Moveable = false;
        StartCoroutine(MoveToPosition(t));

        //Camera.main.GetComponent<CameraController>().ZoomIn(t);
        //Camera.main.GetComponent<CameraController>().Invoke("ZoomIn", 1f);
    }
    int DiveNumber = 0;
    void OnCharacterAnimation(float time)
    {
        //int DiveNumber = Random.Range(1, 5);
        if(DiveNumber == 4)
        {
            DiveNumber = 0;
        }
        DiveNumber++;
        Model.speed = 1;
        Model.speed = Model.speed / (time * 2);
        Model.SetTrigger("Dive"+DiveNumber.ToString());
    }
    public void OnSlowMotion(UnityEngine.UI.Slider slider)
    {
        Time.timeScale = slider.value;
    }
    IEnumerator MoveToPosition(float timeToMove)
    {
        Vector3 currentPos = transform.position;
        float t = 0f;
        float S = transform.position.x-0.05f;
        float v = S / timeToMove;
        while (t < timeToMove)
        {
            t += Time.deltaTime;
            transform.position = new Vector3(transform.position.x - (v*Time.deltaTime), transform.position.y, transform.position.z);
            yield return new WaitForEndOfFrame();
        }
        transform.position = new Vector3(0.05f, transform.position.y, transform.position.z);
    }
}
