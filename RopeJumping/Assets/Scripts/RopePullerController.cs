﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopePullerController : MonoBehaviour
{
    public static RopePullerController Instance;
    public GameObject RopePuller;
    public float RopePullerTarget;

    void Start()
    {
        Instance = this;
    }
    void Update()
    {
        RopePuller.transform.position = new Vector3(RopePuller.transform.position.x,
        Mathf.Lerp(RopePuller.transform.position.y, RopePullerTarget, Time.deltaTime*3),
        RopePuller.transform.position.z); 
    }
    public void DragRope()
    {
        RopePuller.SetActive(true);
    }
    public void StretchRope(float factor)
    {
        LimitInfo limits = RopePuller.GetComponent<LimitInfo>();
        RopePullerTarget = Mathf.Clamp(RopePullerTarget + factor, limits.Min, limits.Max);
        PullAnimation(1 - ((limits.Max - RopePullerTarget) / (limits.Max - limits.Min)));
    }
    public void LeaveRope()
    {
        RopePuller.SetActive(false);
        LimitInfo limits = RopePuller.GetComponent<LimitInfo>();
        RopePuller.transform.position = new Vector3(RopePuller.transform.position.x, limits.Max, RopePuller.transform.position.z);
        RopePullerTarget = limits.Max;
    }
    public void PullAnimation(float factor)
    {
        AssistantController.Instance.RecieveInputs(factor);
    }
}
