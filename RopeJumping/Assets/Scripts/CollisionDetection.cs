﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
    public PlayerController PC;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider collider)
    {
        bool Animateable = false;
        if (IsTriggerInvalid(collider))
        {
            InputController.Instance.Strechable = true;
            InputController.Instance.Moveable = false;
            return;
        }
        InputController.Instance.Strechable = false;
        InputController.Instance.Moveable = true;
        //Debug.Log(collider.GetComponent<Rigidbody>().velocity.y);
        if (collider.GetComponent<Rigidbody>().velocity.y > 10)
        {
            Animateable = true;
        }
        PC.OnCollisionDetach(Animateable);
    }
    void OnTriggerExit(Collider collider)
    {
        if (IsTriggerInvalid(collider))
        {
            InputController.Instance.Strechable = true;
            InputController.Instance.Moveable = false;
            return;
        }
    }
    bool IsTriggerInvalid(Collider collider)
    {
        if(collider.GetComponent<Rigidbody>().velocity.y > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
